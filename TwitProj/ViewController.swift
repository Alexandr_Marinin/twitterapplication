//
//  ViewController.swift
//  TwitProj
//
//  Created by Yasir on 1/1/18.
//  Copyright © 2018 Yacir. All rights reserved.
//

import UIKit
import TwitterKit
import TwitterCore

var userName : String? = ""

class ViewController: UIViewController, TWTRComposerViewControllerDelegate{

    
    
    @IBOutlet var imgTweet: UIImageView!
    @IBOutlet var tvTweet: UITextView!
    
//    var list = ListTimelineViewController()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func btnTwitterSharePressed(_ sender: UIButton) {
        
     
        if (TWTRTwitter.sharedInstance().sessionStore.hasLoggedInUsers()) {
            // App must have at least one logged-in user to compose a Tweet
           
//            guard let shareImg2 = UIImage.init(named: "uk") else{
//                print("failed init share img")
//                return
//            }
            //let shareImg = UIImage.init(named: "mountain")!
            
//            let userName : String
            let composer = TWTRComposerViewController.init(initialText: "UK flag picture will be tweeted", image: nil, videoURL: nil)
            composer.delegate = self
            present(composer, animated: true, completion: nil)
            
        } else {
            // Log in, and then check again
            TWTRTwitter.sharedInstance().logIn { session, error in
                if session != nil { // Log in succeeded
                    
//                    guard let shareImg2 = UIImage.init(named: "usa") else{
//
//                        print("failed init share img")
//                        return
//                    }
                    //let shareImg = UIImage.init(named: "mountain")!
                    userName = session?.userName
                    let composer = TWTRComposerViewController.init(initialText: "USA flag picture will be tweeted", image: nil, videoURL: nil)
                    composer.delegate = self
                    self.present(composer, animated: true, completion: nil)
                } else {
                    let alert = UIAlertController(title: "No Twitter Accounts Available", message: "You must log in before presenting a composer.", preferredStyle: .alert)
                    self.present(alert, animated: false, completion: nil)
                }
            }
            
            
//            if let userID = TWTRTwitter.sharedInstance().sessionStore.session()?.userID {
//            let client = TWTRAPIClient(userID: userID)
//            let client = TWTRAPIClient()
//            let tweetIDs = ["20", "510908133917487104"]
//            client.loadTweets(withIDs: tweetIDs) { (tweets, error) -> Void in
//              // handle the response or error
//            }
        }
        
//        var tweetView = TWTRTweetView()
//        let client = TWTRAPIClient()
//        client.loadTweet(withID: "20") { [weak self] (tweet, error) in
//            if let t = tweet {
//            if let tweetView = self?.tweetView {
//              tweetView.configure(with: t)
//            } else {
//              self?.tweetView = TWTRTweetView(tweet: t, style: TWTRTweetViewStyle.regular)
//            }
//          } else {
//              print("Failed to load Tweet: \(error?.localizedDescription)")
//          }
//        }
    }
    
    //MARK:- TWTRComposerViewControllerDelegate
    
    func composerDidCancel(_ controller: TWTRComposerViewController) {
        print("composerDidCancel, composer cancelled tweet")
    }
    
    func composerDidSucceed(_ controller: TWTRComposerViewController, with tweet: TWTRTweet) {
        print("composerDidSucceed tweet published")
    }
    func composerDidFail(_ controller: TWTRComposerViewController, withError error: Error) {
        print("composerDidFail, tweet publish failed == \(error.localizedDescription)")
    }

}

