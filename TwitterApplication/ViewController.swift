//
//  ViewController.swift
//  TwitterApplication
//
//  Created by Алексей Маринин on 27/10/2019.
//  Copyright © 2019 Sancho. All rights reserved.
//

import UIKit
import TwitterKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        TWTRauth
        let logInButton = TWTRLogInButton(logInCompletion: { session, error in
            if let session = session {
                        print("signed in as \(session.userName)");
                    } else {
                        let errorDescription = error?.localizedDescription ?? "unknown"
                        print("error: \(errorDescription)");
                    }
        })
        logInButton.center = self.view.center
        self.view.addSubview(logInButton)
        
    }


}

